from lcs import *


def test_longest_common_substring():

    longest_common_substring = LongestCommonSubstring('ABABC', 'ABCBA')
    assert(longest_common_substring.solution_length == 3)
    assert(longest_common_substring.solutions == {('A', 'B', 'C')})
    assert(longest_common_substring.matching_indices == {((3, 1), (4, 2), (5, 3))})

    longest_common_substring = LongestCommonSubstring('XMJYAUZ', 'MZJAWXU')
    assert(longest_common_substring.solution_length == 1)
    assert(longest_common_substring.solutions == {('Z',), ('X',), ('U',), ('J',), ('A',), ('M',)})
    assert(longest_common_substring.matching_indices == {((5, 4),), ((7, 2),), ((6, 7),), ((3, 3),), ((2, 1),), ((1, 6),)})


def test_longest_common_subsequence():

    longest_common_substring = LongestCommonSubsequence('ABABC', 'ABCBA')
    assert(longest_common_substring.solution_length == 3)
    assert(longest_common_substring.solutions == {'ABA', 'ABC', 'ABB'})
    assert(longest_common_substring.matching_indices == {
        ((0, 0), (1, 1), (4, 2)), ((0, 0), (3, 1), (4, 2)),
        ((0, 0), (1, 1), (3, 3)), ((0, 0), (1, 3), (2, 4)),
        ((2, 0), (3, 1), (4, 2)), ((0, 0), (1, 1), (2, 4))
    })

    longest_common_substring = LongestCommonSubsequence('XMJYAUZ', 'MZJAWXU')
    assert(longest_common_substring.solution_length == 4)
    assert(longest_common_substring.solutions == {'MJAU'})
    assert(longest_common_substring.matching_indices == {((1, 0), (2, 2), (4, 3), (5, 6))})
