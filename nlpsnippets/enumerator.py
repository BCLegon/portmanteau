from collections import Counter


class Enumerator():

    def __init__(self, items):
        self.counter = Counter(items)
        self.keys_to_items = list(map(lambda x: x[0], self.counter.most_common()))
        self.items_to_keys = dict(map(lambda x: (x[1], x[0]), list(enumerate(self.keys_to_items))))
        self.sorted = True

    def update(self, other):
        self.sorted = False
        self.counter.update(other)
        new_items = set(other) - set(self.items_to_keys)
        insert_index = len(self.keys_to_items)
        self.keys_to_items.extend(new_items)
        self.items_to_keys.update(map(lambda k: (self.keys_to_items[k], k), range(insert_index, insert_index+len(new_items))))

    def items(self):
        return self.keys_to_items

    def keys(self):
        return list(self.items_to_keys.values())

    def key_for(self, item):
        return self.items_to_keys[item]

    def item_for(self, key):
        return self.keys_to_items[key]

    def get_key_frequency(self, key):
        return self.counter[self.keys_to_items[key]]

    def get_item_frequency(self, item):
        return self.counter[item]

    def key_more_frequent(self, key_a, key_b):
        if self.sorted:
            return key_a > key_b
        else:
            return self.counter[self.keys_to_items[key_a]] > \
                   self.counter[self.keys_to_items[key_b]]

    def item_more_frequent(self, item_a, item_b):
        return self.counter[item_a] > self.counter[item_b]
