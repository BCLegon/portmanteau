class LongestCommonProblem:

    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.matrix_shape = (len(a) + 1, len(b) + 1)
        self._length_matrix = None
        self._match_matrix = None
        self._solutions = None
        self._solution_length = None
        self._matching_indices = None

    @property
    def length_matrix(self):
        if self._length_matrix is None:
            self._length_matrix, self._match_matrix = self._generate_matrices()
        return self._length_matrix

    @property
    def match_matrix(self):
        if self._match_matrix is None:
            self._length_matrix, self._match_matrix = self._generate_matrices()
        return self._match_matrix

    def _generate_matrices(self):
        raise NotImplementedError

    @property
    def solution_length(self):
        if self._solution_length is None:
            self._solution_length = self._generate_solution_length()
        return self._solution_length

    def _generate_solution_length(self):
        raise NotImplementedError

    @property
    def solutions(self):
        if self._solutions is None:
            self._solutions = self._generate_solutions()
        return self._solutions

    def _generate_solutions(self):
        raise NotImplementedError

    @property
    def matching_indices(self):
        if self._matching_indices is None:
            self._matching_indices = self._generate_matching_indices()
        return self._matching_indices

    def _generate_matching_indices(self):
        raise NotImplementedError


class LongestCommonSubstring(LongestCommonProblem):

    def _generate_matrices(self):
        length_matrix = [[0] * self.matrix_shape[1] for _ in range(self.matrix_shape[0])]
        match_matrix = [[0] * self.matrix_shape[1] for _ in range(self.matrix_shape[0])]
        for i in range(1, self.matrix_shape[0]):
            for j in range(1, self.matrix_shape[1]):
                if self.a[i - 1] == self.b[j - 1]:
                    length_matrix[i][j] = length_matrix[i - 1][j - 1] + 1
                    match_matrix[i][j] = 1
        return length_matrix, match_matrix

    def _generate_solution_length(self):
        return max([max(length_row) for length_row in self.length_matrix])

    def _generate_solutions(self):
        substrings = set()
        for match in self.matching_indices:
            substrings.add(tuple([self.a[loc[0] - 1] for loc in match]))
        return substrings

    def _generate_matching_indices(self):
        matches = set()
        for row in range(self.matrix_shape[0]):
            for col in range(self.matrix_shape[1]):
                if self.length_matrix[row][col] == self.solution_length:
                    matches.add(tuple([(row - i, col - i) for i in range(self.solution_length - 1, -1, -1)]))
        return matches


class LongestCommonSubsequence(LongestCommonProblem):

    def _generate_matrices(self):
        # Adapted from https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_subsequence#Python
        length_matrix = [[0] * self.matrix_shape[1] for _ in range(self.matrix_shape[0])]
        match_matrix = [[0] * self.matrix_shape[1] for _ in range(self.matrix_shape[0])]
        for i in range(1, self.matrix_shape[0]):
            for j in range(1, self.matrix_shape[1]):
                if self.a[i - 1] == self.b[j - 1]:
                    length_matrix[i][j] = length_matrix[i - 1][j - 1] + 1
                    match_matrix[i][j] = 1
                else:
                    length_matrix[i][j] = max(length_matrix[i][j - 1], length_matrix[i - 1][j])
        return length_matrix, match_matrix

    def _generate_solution_length(self):
        return self.length_matrix[self.matrix_shape[0] - 1][self.matrix_shape[1] - 1]

    def _generate_solutions(self, row=None, col=None):
        if row is None: row = len(self.length_matrix) - 1
        if col is None and row >= 0: col = len(self.length_matrix[0]) - 1

        if self.length_matrix[row][col] == 0 or row <= 0 or col <= 0:
            # No matches from here.
            return {''}

        subsequences = set()
        if self.match_matrix[row][col] == 1:
            # Matching character. Move diagonally.
            return set([subsequence + self.a[row - 1] for subsequence in self._generate_solutions(row - 1, col - 1)])
        if self.length_matrix[row][col] == self.length_matrix[row - 1][col]:
            # Score origin might be up. Move upwards.
            subsequences.update(self._generate_solutions(row - 1, col))
        if self.length_matrix[row][col] == self.length_matrix[row][col - 1]:
            # Score origin might be left. Move left.
            subsequences.update(self._generate_solutions(row, col - 1))
        return subsequences

    def _generate_matching_indices(self, row=None, col=None):
        if row is None: row = len(self.length_matrix) - 1
        if col is None and row >= 0: col = len(self.length_matrix[0]) - 1

        if self.length_matrix[row][col] == 0 or row <= 0 or col <= 0:
            # No matches from here.
            return {()}

        matches = set()
        if self.match_matrix[row][col] == 1:
            # Matching character. Move diagonally.
            new_match = (row - 1, col - 1)
            partial_matches = self._generate_matching_indices(row - 1, col - 1)
            matches = set([partial_match + (new_match,) for partial_match in partial_matches])
        if self.length_matrix[row][col] == self.length_matrix[row - 1][col]:
            # Score origin might be up. Move upwards.
            matches.update(self._generate_matching_indices(row - 1, col))
        if self.length_matrix[row][col] == self.length_matrix[row][col - 1]:
            # Score origin might be left. Move left.
            matches.update(self._generate_matching_indices(row, col - 1))

        return matches
