from nlpsnippets.lcs import LongestCommonSubstring


class Portmanteau:

    def __init__(self, a, b):
        self.a = a
        self.b = b
        self._solutions = None
        self._best_solution = None
        self._best_score = None

    def __str__(self):
        return self.solution_to_string(self.best_solution)

    def solution_to_string(self, paired_indices):
        solution = ''
        for pair in paired_indices:
            if pair[0] is not None:
                solution += str(self.a[pair[0]])
            elif pair[1] is not None:
                solution += str(self.b[pair[1]])
        return solution

    def _generate_possible_cores(self):
        longest_common_substring = LongestCommonSubstring(self.a, self.b)
        indices = set()
        for match in longest_common_substring.matching_indices:
            indices.add(tuple([(c_1 - 1, c_2 - 1) for (c_1, c_2) in match]))
        return indices

    def _generate_possible_corners(self, core):
        corner_top, corner_bottom = 0, len(self.a)
        corner_left, corner_right = 0, len(self.b)

        if len(core) > 0:
            vertical_indices, horizontal_indices = zip(*core)
            core_top, core_bottom = min(vertical_indices), max(vertical_indices) + 1
            core_left, core_right = min(horizontal_indices), max(horizontal_indices) + 1
        else:
            core_top, core_bottom = corner_bottom, corner_top
            core_left, core_right = corner_right, corner_left

        top_to_right   = self._semi_indexed_range(corner_top, core_top, 0) + \
                         core + self._semi_indexed_range(core_right, corner_right, 1)
        left_to_bottom = self._semi_indexed_range(corner_left, core_left, 1) + \
                         core + self._semi_indexed_range(core_bottom, corner_bottom, 0)
        return [top_to_right, left_to_bottom]

    @staticmethod
    def _extract_core(corner):
        return tuple([(a_ind, b_ind) for a_ind, b_ind in corner if a_ind is not None and b_ind is not None])

    @property
    def solutions(self):
        if self._solutions is None:
            self._solutions = self._generate_solutions()
        return self._solutions

    def _generate_solutions(self):
        solutions = set()
        for core in self._generate_possible_cores():
            for corner in self._generate_possible_corners(core):
                solutions.add(corner)
        return solutions

    @property
    def best_solution(self):
        if self._best_solution is None:
            self._best_solution, self._best_score = self._get_best_solution_score()
        return self._best_solution

    @property
    def best_score(self):
        if self._best_score is None:
            self._best_solution, self._best_score = self._get_best_solution_score()
        return self._best_score

    def _get_best_solution_score(self):
        best_score = 0
        best_solution = None
        for solution in self.solutions:
            score = self.get_solution_score(solution)
            if score > best_score:
                best_score = score
                best_solution = solution
        return best_solution, best_score

    @staticmethod
    def get_solution_score(paired_indices):
        core_score = Portmanteau._get_solution_area(
            Portmanteau._extract_core(paired_indices)
        )
        corner_score = Portmanteau._get_solution_area(paired_indices)
        total_score = corner_score + core_score  # core_score is implicitly counted twice
        return total_score

    @staticmethod
    def _get_solution_area(paired_indices):
        if len(paired_indices) == 0:
            return 0

        a_indices, b_indices = zip(*paired_indices)
        a_indices = [ind for ind in a_indices if ind is not None]
        b_indices = [ind for ind in b_indices if ind is not None]

        a_score = max(a_indices) + 1 - min(a_indices)
        b_score = max(b_indices) + 1 - min(b_indices)
        area = a_score * b_score
        return area

    @staticmethod
    def _semi_indexed_range(start, stop, col):
        if col == 0:
            return tuple(zip(range(start, stop), [None] * (stop - start)))
        else:
            return tuple(zip([None] * (stop - start), range(start, stop)))
