# Portmanteau

[![pipeline status](https://gitlab.com/BCLegon/portmanteau/badges/portmanteau_master/pipeline.svg)](https://gitlab.com/BCLegon/portmanteau/commits/portmanteau_master)
[![coverage report](https://gitlab.com/BCLegon/portmanteau/badges/portmanteau_master/coverage.svg)](https://gitlab.com/BCLegon/portmanteau/commits/portmanteau_master)

Application for generating [portmanteau words](https://en.wikipedia.org/wiki/Portmanteau).


## Example

```python
from portmanteau import Portmanteau

brandela = Portmanteau('mandela', 'brand')
print(brandela)  # outputs 'brandela'
```
