from portmanteau import *


def test_brandela():

    brandela = Portmanteau('mandela', 'brand')

    assert(((None, 0), (None, 1), (1, 2), (2, 3), (3, 4), (4, None), (5, None), (6, None)) in brandela.solutions)
    assert(((0, None), (1, 2), (2, 3), (3, 4)) in brandela.solutions)
    assert(str(brandela) == 'brandela')


def test_brangelina():

    brangelina = Portmanteau('brad', 'angelina')

    assert(((0, None), (1, None), (2, 0), (None, 1), (None, 2), (None, 3), (None, 4), (None, 5), (None, 6), (None, 7)) in brangelina.solutions)
    assert(str(brangelina) == 'brangelina')


def test_larnout():

    larnout = Portmanteau('lara', 'arnout')

    assert(((0, None), (1, 0), (2, 1), (None, 2), (None, 3), (None, 4), (None, 5)) in larnout.solutions)
    assert(str(larnout) == 'larnout')


def test_californication():

    californication = Portmanteau('California', 'fornication')

    assert(((0, None), (1, None), (2, None), (3, None), (4, 0), (5, 1), (6, 2), (7, 3), (8, 4), (None, 5), (None, 6), (None, 7), (None, 8), (None, 9), (None, 10)) in californication.solutions)
    assert(str(californication) == 'Californication')


def test_billary():

    billary = Portmanteau('bill', 'hillary')

    assert(((0, None), (1, 1), (2, 2), (3, 3), (None, 4), (None, 5), (None, 6)) in billary.solutions)
    assert(str(billary) == 'billary')


def test_smog():

    smog = Portmanteau('smoke', 'fog')

    assert(((0, None), (1, None), (2, 1), (None, 2)) in smog.solutions)
    assert(str(smog) == 'smog' or str(smog) == 'foke')
